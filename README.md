## Bot Agent 

### Description
Here you find the files that make a bot and train it to understand unstructured data

1. data: 
*   nlu: Contains the training data used to train the bot NLU component 
            
*   stories : The possible paths a conversation can take, training data for TED policy transfomer 